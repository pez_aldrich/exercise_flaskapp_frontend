from flask import Flask 

app = Flask(__name__)

from app import views 
from app import contact_views
from app import about_views